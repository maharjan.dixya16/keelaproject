
Cypress.Commands.add('loginWithoutUI', (credentials) => {
  cy.request('/web/login').then((response) => {
    const $html = Cypress.$(response.body);
    const $csrf = $html.find('input[name=csrf_token]').val();

    cy.request({
      method: 'POST',
      url: '/web/login',
      form: true,
      body: {
        csrf_token: $csrf,
        login: credentials.login,
        password: credentials.password,
      },
    });
  });
});