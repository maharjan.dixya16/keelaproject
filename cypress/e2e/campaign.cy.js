/// <reference types='Cypress' />
describe('Keela Test Suites', function () {
  let credentials, campaign;

  before(() => {
    cy.fixture('login').then((data) => {
      credentials = data;
    });
    cy.fixture('campaign').then((data) => {
      campaign = data;
    });

  });

  beforeEach(() => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
  });

  it('should add a new campaign', function () {

    cy.visit('dev.keela.co');
    cy.get('#user-email').type(credentials.email);
    cy.get('#user-password').type(credentials.password);
    cy.get('.btn-block').click();
    cy.get('#sidebar').within(() => {
      cy.get('[data-tracking-navigation="Campaigns"]').click();
    });
    cy.get('[data-tracking-button="campaigns-add-campaign"]').click();
    cy.get('[data-tracking-input="campaigns-Campaigns-name"] input').type(campaign.campaignName);
    cy.get('[data-tracking-input="campaigns-Campaigns-description"] textarea').type(campaign.descriptionField);
    cy.get('[data-tracking-input="campaigns-Campaigns-fundraisingGoal"] input').type(campaign.goal);
    cy.get('[data-tracking-button="add-campaign-modal-save"]').click();
  });

  it('should edit a campaign', function () {
    cy.visit('dev.keela.co');
    cy.get('#user-email').type(credentials.email);
    cy.get('#user-password').type(credentials.password);
    cy.get('.btn-block').click();
    cy.get('#sidebar').within(() => {
      cy.get('[data-tracking-navigation="Campaigns"]').click();
    });
    cy.get('[data-tracking-table="campaigns-Campaigns-0-0"]').click();
    cy.get('[data-tracking-button="campaign-overview-actions"]').click();
    cy.get('.dropdown-item').contains('Edit Information').click();
    cy.get('[data-tracking-input="campaign-overview-Campaigns-description"] textarea').clear().type(campaign.updateGoal);
    cy.get('[data-tracking-button="add-campaign-modal-save"]').click();
    cy.get('.px-0 .undefined').should('include.text', campaign.updateGoal);

  });

  it('should delete a campaign', function () {
    cy.visit('dev.keela.co');
    cy.get('#user-email').type(credentials.email);
    cy.get('#user-password').type(credentials.password);
    cy.get('.btn-block').click();
    cy.get('#sidebar').within(() => {
      cy.get('[data-tracking-navigation="Campaigns"]').click();
    });
    cy.get('[data-tracking-table="campaigns-Campaigns-0-0"]').click();
    cy.get('[data-tracking-button="campaign-overview-actions"]').click();
    cy.get('.dropdown-item').contains('Delete').click();
    cy.get('.push .font-w500').first().invoke('text').as('campaignName');
    cy.get('.swal2-content .swal2-input').type('DELETE');
    cy.contains('Confirm').click();
    cy.get('@campaignName').then((value) => {
      cy.get('[data-tracking-table="campaigns-Campaigns-0-0"]').should('not.include.text', value)
    });
  });
});
