/// <reference types='Cypress' />
describe('Keela Test Suites', function () {
  let credentials, value;

  before(() => {
    cy.fixture('login').then((data) => {
      credentials = data;
    });
    cy.fixture('contacts').then((data) => {
      value = data;
    });

  });

  beforeEach(() => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      return false;
    });
  });

  it('should add a contact', function () {
    cy.visit('dev.keela.co');
    cy.get('#user-email').type(credentials.email);
    cy.get('#user-password').type(credentials.password);
    cy.get('.btn-block').click();
    cy.get('#sidebar').within(() => {
      cy.get('[data-tracking-navigation="Contacts"]').click();
    });
    cy.get('[data-tracking-button="contacts-add-contact"]').click();
    cy.get('[data-tracking-input="contacts-PersonContacts-firstName"] input').type(value.firstName);
    cy.get('[data-tracking-input="contacts-PersonContacts-lastName"] input').type(value.lastName);
    cy.get('[data-tracking-input="contacts-PersonContacts-primaryEmail"] input').type(value.email);
    cy.get('[ data-tracking-input="contacts-PersonContacts-primaryPhone"] input').type(value.phoneNumber);
    cy.get('[data-tracking-input="contacts-PersonContacts-birthDate"]').within(() => {
      cy.get('select').eq(0).select('1');
      cy.get('select').eq(1).select('February');
      cy.get('select').eq(2).select('2023');
    });
    cy.get('[data-tracking-button="add-person-contact-modal-save"]').click();
  });

  it('should edit contact', function () {
    cy.visit('dev.keela.co');
    cy.get('#user-email').type(credentials.email);
    cy.get('#user-password').type(credentials.password);
    cy.get('.btn-block').click();
    cy.get('#sidebar').within(() => {
      cy.get('[data-tracking-navigation="Contacts"]').click();
    });
    cy.get('[data-tracking-table="contacts-Contacts-0-0"]').click();
    cy.contains('Personal Info').should('be.visible');
    cy.get('.block-options').eq(1).contains('Edit').click();
    cy.get('[data-tracking-input="contact-profile-PersonContacts-title"] input').type('Mr.');
    cy.get('[data-tracking-input="contact-profile-PersonContacts-lastName"]').click();
    cy.get('[ data-tracking-input="contact-profile-PersonContacts-middleName"] input').type(value.middleName);
    cy.get('[data-tracking-button="edit-contact-modal-save"]').click();
  });

  it('should delete contact', function () {
    cy.visit('dev.keela.co');
    cy.get('#user-email').type(credentials.email);
    cy.get('#user-password').type(credentials.password);
    cy.get('.btn-block').click();
    cy.get('#sidebar').within(() => {
      cy.get('[data-tracking-navigation="Contacts"]').click();
    });
    cy.get('[data-tracking-table="contacts-Contacts-0-0"]').invoke('text').as('contactname');
    cy.get('.custom-checkbox').eq(0).click();
    cy.get('[data-tracking-button="contacts-actions"]').click();
    cy.get('.dropdown-item').contains('Delete').click();
    cy.get('.swal2-content .swal2-input').type('DELETE');
    cy.contains('Confirm').click();
    cy.get('@contactname').then((value) => {
      console.log(value);
      cy.get('[data-tracking-table="contacts-Contacts-0-0"]').should('not.include.text', value)
    });
  });
});